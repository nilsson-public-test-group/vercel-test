/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  swcMinify: true,
  sassOptions: {
    additionalData: `@import "src/styles/utils/media-queries.scss"; @import "src/styles/utils/variables.scss";`,
  },
  images: {
    remotePatterns: [
      {
        protocol: `https`,
        hostname: process.env.BACKEND_HOST,
        port: ``,
        pathname: `/sites/default/files/**`,
      },
    ],
  },
  async rewrites() {
    return [
      {
        source: `/sites/default/files/:path*`,
        destination: `https://${ process.env.BACKEND_HOST }/sites/default/files/:path*`,
      },
      {
        source: `/sitemap.xml`,
        destination: `https://${ process.env.BACKEND_HOST }/sitemap.xml`,
      },
    ]
  }
}

export default nextConfig;
