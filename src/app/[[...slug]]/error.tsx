"use client";

const Error = () => {
  return (
    <p>Er ging iets mis bij het ophalen van de gegevens, ververs deze pagina of probeer het later opnieuw.</p>
  )
}

export default Error;
