const NotFound = () => {
  return (
    <div>
      <h1>Pagina niet gevonden</h1>
      <p>De opgevraagde pagina bestaat niet (meer) of is verplaatst. Misschien kunt u via het menu vinden wat u zoekt?</p>
    </div>
  )
}


export default NotFound;
