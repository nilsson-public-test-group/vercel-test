import usePageInfo from "features/node/features/page/hooks/usePageInfo";
import { Drupal } from "utils/drupal/NilssonDrupal";
import { notFound } from "next/navigation";
import NilssonStatic from "utils/drupal/NilssonStatic";

type Props = {
  params: {
    slug: string[],
  },
  searchParams: {
    [key: string]: string
  }
}

type TIncludedReferences = {
  [index: string]: string[];
}

const includedReferences: TIncludedReferences = {
  page: [
    `field_hero`,
    `field_hero.field_media_hero`,
  ],
  news: [
    `field_image`,
    `field_image.field_media_image`,
  ],
}

const AppSlugPage = async ({ params, searchParams }: Props) => {
  const path = params.slug?.join(`/`) || `/`;
  const pageInfo = await usePageInfo(path, true);

  if (!pageInfo) {
    Drupal.log(`Could not load pageInfo for path ${ path }`, `error`);
    return notFound();
  }

  await Drupal.init(path);

  const entity = await Drupal.getEntity(
    `node`,
    pageInfo.entity.bundle,
    pageInfo.entity.uuid,
    includedReferences[pageInfo.entity.bundle]
  );

  return (
    <>
      <h1>Slug: { params?.slug?.join(`/`) }</h1>
      <h2>{ entity.title }</h2>
    </>
  )
}

export const generateStaticParams = async () => await NilssonStatic.getStaticParams();


export default AppSlugPage;
