import { Authenticator } from "utils/drupal/NilssonAuthenticator";
import { Format } from "utils/misc/Format";
import * as process from "process";

class NilssonDrupal {

  private currentPath: string = ``;

  /**
   * @param path
   */
  public init = async (path: string): Promise<void> => {
    this.currentPath = path;
  }

  /**
   * Returns the current request path.
   */
  public getCurrentPath = (): string => {
    return this.currentPath;
  }

  /**
   * Returns the base path of the backend API.
   *
   * @param languageDependend
   */
  public getBasePath = (languageDependend: boolean = true): string => {
    return `https://${ process.env.BACKEND_HOST }`;
  }

  /**
   * @param url
   */
  public async fetchData(url: string): Promise<any> {
    const token = await Authenticator.authenticate();
    const params = {
      headers: {
        Authorization: `Bearer ${ token }`,
      },
    };
    const response = await fetch(url, params);

    let json = false;
    if (response.ok) {
      json = await response.json();
    } else {
      this.log(`Failed to load JSON from ${ url }`, `error`);
    }

    return json;
  }

  /**
   * @param url
   * @param data
   */
  public async postData(url: string, data: object): Promise<Response> {
    this.log(`Posting data to path ${ url }`);

    const token = await Authenticator.authenticate();
    const params = {
      headers: {
        "Authorization": `Bearer ${ token }`,
        "Content-Type": `application/json`,
      },
      method: `POST`,
      body: JSON.stringify(data),
    };
    const response = await fetch(url, params);

    if (![200, 201, 202, 203].includes(response.status)) {
      this.log(`Failed to POST data to ${ url } (${ response.status }: ${ response.statusText })`, `error`, response)
    }

    return response;
  }

  /**
   * Returns information about the page behind the given path.
   *
   * @param path
   */
  public async getPageInfo(path: string): Promise<TPageInfo> {
    const url = this.getBasePath() + `/router/translate-path?path=${ path }`;
    this.log(`Requesting route info from path ${ url }`);
    const result = await this.fetchData(url);
    console.log(`Returned route info from ${ url }`, result);
    return result;
  }

  /**
   * Returns an entity from the backend.
   *
   * @param type
   * @param bundle
   * @param uuid
   * @param includeFields
   */
  public async getEntity(
    type: string,
    bundle: string,
    uuid: string,
    includeFields: string[] | null = null
  ): Promise<any> {
    let url = this.getBasePath() + `/api/${ type }/${ bundle }/${ uuid }?resourceVersion=rel:latest-version`;
    if (includeFields) {
      url = `${ url }&include=${ includeFields.join(`,`) }`;
    }
    this.log(`Loading entity data from ${ url }`);
    const result = await this.fetchData(url);
    console.log(`Returned entity data from ${ url }`, result);
    return result?.data as TNode || null;
  }

  /**
   * Returns the menu items for a menu.
   *
   * @param id
   * @param sortField
   * @param sortDirection
   */
  public async getMenu(id: string, sortField: string = `weight`, sortDirection: `ASC` | `DESC` = `ASC`): Promise<{
    data: TMenuItemData[]
  } | null> {
    const sort = `&sort[${ sortField }][path]=${ sortField }&sort[${ sortField }][direction]=${ sortDirection }`;
    const url = this.getBasePath() + `/api/menu_link_content/menu_link_content?filter[menu_name]=${ id }&filter[enabled]=1${ sort }`;
    this.log(`Loading menu data from ${ url }`);
    return await this.fetchData(url);
  }

  /**
   *
   * Log messages including timestamps.
   *
   * @param message
   * @param type
   * @param data
   */
  public log = (message: string, type: `info` | `warning` | `error` = `info`, data: any = ``) => {
    const date = new Date();
    const dateTimeString = Format.date(date.getTime(), `date-time`);
    const logMessage = `[${ type }] ${ message } (${ dateTimeString })`;
    switch (type) {
      case `error`:
        console.warn(logMessage, data);
        break;
      case `warning`:
        console.warn(logMessage, data);
        break;
      default:
        console.info(logMessage, data);
        break;
    }
  }

}

export const Drupal = new NilssonDrupal();
