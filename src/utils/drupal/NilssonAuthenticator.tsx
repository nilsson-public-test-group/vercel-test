import { Drupal } from "utils/drupal/NilssonDrupal";
import { Format } from "utils/misc/Format";

class NilssonAuthenticator {

  private token: TToken | null = null;

  /**
   * Returns an oAuth authentication token.
   */
  public authenticate = async (): Promise<string | null> => {
    if (this.mustRefreshToken()) {
      this.token = await this.getToken();
    }

    return this.token?.access_token || null;
  }

  /**
   * Returns true if the current token has expired and needs
   * to be refreshed.
   */
  public mustRefreshToken(): boolean {
    if (!this.token) {
      return true;
    }
    const date = new Date();
    const milliSecondsLeft = this.token.expires - date.getTime();
    return milliSecondsLeft <= 5000; // keep a few seconds margin before actual expiration.
  }

  /**
   * Returns oAuth authentication data.
   */
  private getToken = async () => {
    const url = `https://${ process.env.BACKEND_HOST }/oauth/token`;

    const data = new URLSearchParams();
    data.append(`username`, `${ process.env.BACKEND_USERNAME }`);
    data.append(`password`, `${ process.env.BACKEND_PASSWORD }`);
    data.append(`client_id`, `${ process.env.BACKEND_CLIENT_ID }`);
    data.append(`client_secret`, `${ process.env.BACKEND_CLIENT_SECRET }`);
    data.append(`grant_type`, `password`);
    data.append(`scope`, `api`);

    let response;
    const params = {
      method: `POST`,
      body: data,
      next: {
        revalidate: 0,
      }
    }

    try {
      response = await fetch(url, params);
    } catch (exception) {
      throw new Error(`Fatal authentication error: ${ exception }`);
    }

    if (!response?.ok) {
      throw new Error(`Authentication failure: ${ response.statusText } (${ response.status }`);
    }

    const result = await response?.json();
    if (!result) {
      Drupal.log(`Failed to get an authentication token from the response.`, `error`);
      return null;
    }

    result.expires = new Date().getTime() + (result.expires_in * 1000)
    Drupal.log(`Got new authentication data, expires ${ Format.date(result.expires, `date-time`) }`, `info`);

    return result || null;
  }
}

export const Authenticator = new NilssonAuthenticator();
