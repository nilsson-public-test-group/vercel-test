import { Drupal } from "utils/drupal/NilssonDrupal";

class NilssonStatic {

  public static getStaticParams = async (): Promise<object[]> => {
    const mainMenuId = `main`;
    const mainMenu = await Drupal.getMenu(mainMenuId);

    if (!mainMenu?.data) {
      Drupal.log(`Could not load menu "${ mainMenuId }" to generate static pages.`, `error`);
      return [];
    }

    let slugs: object[] = [];
    mainMenu.data.forEach((item: TMenuItemData) => {
      const url = item.link.url;
      if (this.isValidUrl(url)) {
        const slug = this.urlToSlug(url);
        slugs.push({
          slug: slug
        });
      }
    });

    return slugs;
  }

  private static isValidUrl(url: string): boolean {
    if (url === `/`) {
      return false;
    }

    return url.charAt(0) === `/`;
  }

  private static urlToSlug = (url: string): string[] => {
    let urlAsArray = url.substring(1).split(`/`);

    if (!Array.isArray(urlAsArray)) {
      urlAsArray = new Array(urlAsArray);
    }

    return urlAsArray;
  }

}

export default NilssonStatic;
