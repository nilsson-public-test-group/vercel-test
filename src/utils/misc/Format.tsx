export class Format {

  public static price = (value: number): string => {
    let euroFormatter = Intl.NumberFormat(`nl-NL`, {
      style: `currency`,
      currency: `EUR`,
    });

    let result = euroFormatter.format(value);

    return result.replace(`,00`, `,-`);
  }

  public static date = (value: string | number, format: `custom` | `date` | `time` | `date-time` = `date`, options: {} = {}) => {
    switch (format) {
      case `time`:
        options = {
          hour: `numeric`,
          minute: `numeric`,
        }
        break;
      case `date-time`:
        options = {
          day: `numeric`,
          month: `long`,
          year: `numeric`,
          hour: `numeric`,
          minute: `numeric`,
        };
        break;
      default:
        options = {
          day: `numeric`,
          month: `long`,
          year: `numeric`
        };
        break;
    }
    const date: Date = new Date(value);
    return date.toLocaleDateString(`nl-NL`, options);
  }

}
