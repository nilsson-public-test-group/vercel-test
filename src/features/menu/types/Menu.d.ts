type TMenuItemData = {
  id: string,
  title: string,
  link: TLink,
  itemClass: string,
  linkClass: string,
  options: {
    target?: string
  },
  parent: string,
  enabled: boolean
}
