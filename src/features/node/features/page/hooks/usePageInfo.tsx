import { notFound, redirect } from "next/navigation";
import { Drupal } from "utils/drupal/NilssonDrupal";

const usePageInfo = async (
  path: string,
  allowRedirects: boolean = true
): Promise<TPageInfo | null> => {
  const pageInfo = await Drupal.getPageInfo(path);

  if (!pageInfo?.resolved) {
    if (allowRedirects) {
      notFound();
    }
    else {
      return null;
    }
  }

  if (pageInfo.redirect && allowRedirects) {
    const redirectTo = pageInfo.redirect[0]?.to;
    Drupal.log(`Redirect found from ${ path } to ${ redirectTo }.`);
    redirect(redirectTo);
  }

  return pageInfo;
}

export default usePageInfo;
