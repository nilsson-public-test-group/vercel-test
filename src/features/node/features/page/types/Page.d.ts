type TNodePage = TNode & {
  field_content: TParagraph[];
  field_template: string;
}
