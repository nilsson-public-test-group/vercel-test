type TNode = {
  id: number;
  drupal_internal__nid: number;
  drupal_internal__vid: number;
  type: string;
  status: boolean;
  title: string;
  field_hero?: TMediaHero;
  created: number;
  changed?: number;
  promote: boolean;
  sticky: boolean;
  metatag: TMetatag[];
  path: TPath;
  breadcrumb?: TLink[];
  data: [];
}
