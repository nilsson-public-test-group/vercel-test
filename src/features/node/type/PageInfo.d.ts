type TPageInfo = {
  resolved: string;
  isHomePath: boolean;
  entity: {
    canonical: string;
    type: string;
    bundle: string;
    id: string;
    uuid: string;
  };
  label: string;
  jsonapi: {
    individual: string;
  };
  redirect?: [
    {
      from: string;
      to: string;
      status: string;
    }
  ];
}
