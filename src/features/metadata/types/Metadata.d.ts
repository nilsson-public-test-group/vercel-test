type TMetatag = {
  tag: string;
  attributes: {
    name?: string;
    rel?: string;
    href?: string;
    content?: string;
    property?: string;
  }
}

type TMetadataProps = {
  params: {
    slug: string[],
  },
}
